# Expression Builder
## Gerador de expressões

Material de estudo para expressões, o objetivo é alcançar um mecanismo para interfaces que permitem ao usuário gerar suas próprias condições através do uso de expressões e LINQ


## Recursos

- Expressões recursivas

## Caso de uso

![alt text](PrintAplicacao/sources/overview.png "Exemplo de uso")
