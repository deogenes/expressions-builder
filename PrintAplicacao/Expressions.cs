﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PrintAplicacao
{
    public enum ExpressionType
    {
        EqualExpression,
        ConstantExpression,
        Field
    }

    public class UiExpression
    {
        public string ParameterType;
        public object ParameterValue;

        public ExpressionType ExpressionType;
        public UiExpression? RightValue;
    }

    public class PreExpression
    {
        public Expression left;
        public Expression? right;
    }

    public class ExpressionFactory
    {
        public IExpression Create(ExpressionType expressionType)
        {
            //if(expressionType == ExpressionType.EqualExpression)
            //  new EqualExpression()

            return null;
        }
    }

    public class EqualExpression : IExpression
    {
        protected Expression Left, Right;

        public EqualExpression(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public Expression Generate()
        {
            return Expression.Equal(Left, Right);
        }
    }

    public class ConstantExpression : IExpression
    {
        private object _value;

        public ConstantExpression(object value)
        {
            _value = value;
        }

        public Expression Generate()
        {
            return Expression.Constant(_value);
        }
    }
}
