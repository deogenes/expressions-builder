﻿using PrintAplicacao;
using System.Linq.Expressions;
using ExpressionType = PrintAplicacao.ExpressionType;

var uiExpressionOne = new UiExpression()
{
    ExpressionType = ExpressionType.EqualExpression,
    ParameterType = "Pessoa",
    ParameterValue = "Nome",
    RightValue = new UiExpression()
    {
        ExpressionType = ExpressionType.Field,
        ParameterType = "Pessoa",
        ParameterValue = "Nome"
    }
};

var uiExpressionTwo = new UiExpression()
{
    ExpressionType = ExpressionType.EqualExpression,
    ParameterType = "Pessoa",
    ParameterValue = "Nome",
    RightValue = new UiExpression()
    {
        ExpressionType = ExpressionType.ConstantExpression,
        ParameterType = "string",
        ParameterValue = "Nicoletti"
    }
};

var andLambdaFactory = new AndLambdaExpression();
var expression = (Expression<Func<Pessoa, bool>>)andLambdaFactory.Generate(new UiExpression[] { uiExpressionOne, uiExpressionTwo });

var lista = new List<Pessoa>() {
    new Pessoa(){ Nome = "Fulano", Sobrenome = "Deltrano" },
    new Pessoa(){ Nome = "Nicoletti", Sobrenome = "Nicoletti" }
};

Console.WriteLine($"\n\nLista: {String.Join(',', lista.Select(x => x.Nome) )}");
lista = lista.AsQueryable().Where(expression).ToList();
Console.WriteLine($"\n\nA expressão \n{expression} \n\nprovocou os resultados na lista: \n {String.Join(',', lista.Select(x => x.Nome)  )}");


//Expressão que desejo alcançar
Expression<Func<string>> expr = () => "fulano";

public class Pessoa
{
    public int Id { get; set; }

    public string Sobrenome { get; set; } = string.Empty;

    public string Nome { get; set; } = string.Empty;
}