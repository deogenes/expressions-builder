﻿using System;
using System.Linq.Expressions;

namespace PrintAplicacao
{
    public interface ILambdaExpressionFactory
    {
        LambdaExpression Generate(UiExpression[] uiExpressions);
    }

    public interface IExpression
    {
        public Expression Generate();
    }


}
