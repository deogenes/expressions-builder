﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Reflection.Metadata;
using System.Data.Common;

namespace PrintAplicacao
{
    public abstract class LambdaExpressionBase : ILambdaExpressionFactory
    {
        protected List<ParameterExpression> exprParameters = new List<ParameterExpression>();
        internal class ParameterNameGenerator
        {
            protected ushort ParameterSequence = 0;

            public string Execute(Type paramType)
            {
                var firstLetter = paramType.Name.First().ToString().ToLower();
                return $"{firstLetter}";
            }
        }

        internal ParameterNameGenerator paramNameGenerator = new ParameterNameGenerator();

        public abstract LambdaExpression Generate(UiExpression[] uiExpressions);

        protected InvocationExpression Generate(UiExpression uiExpression)
        {
            var exprBodyExpression = GenerateLabdaExpr(uiExpression, ref exprParameters);

            var lambda = Expression.Lambda(exprBodyExpression, exprParameters);
            var invokedExpr = Expression.Invoke(lambda, exprParameters);

            //Console.WriteLine($"Parametros: {String.Join(',', exprParameters.Select(x => x.ToString()))}");
            //Console.WriteLine($"Expressão: {exprBodyExpression}");
            Console.WriteLine($"Expressão invocável: {invokedExpr}");

            return invokedExpr;
        }

        protected Expression GenerateLabdaExpr(UiExpression uiExpression, ref List<ParameterExpression> exprParameters)
        {
            Expression rightExpression = null;

            if (uiExpression.RightValue != null)
                rightExpression = GenerateLabdaExpr(uiExpression.RightValue, ref exprParameters);

            var leftExpression = GenerateExpression(uiExpression: uiExpression, right: rightExpression, ref exprParameters);
            return leftExpression;
        }

        public Expression GenerateExpression(UiExpression uiExpression, Expression right, ref List<ParameterExpression> exprParameters)
        {
            var isConcreteType = Type.GetType(uiExpression.ParameterType) != null;
            var parameterType = Type.GetType(uiExpression.ParameterType) ?? uiExpression.ParameterValue.GetType();
            var exprParamName = paramNameGenerator.Execute(parameterType);

            ParameterExpression? exprParameter = exprParameters.Find(x => x.Type.Equals(parameterType));

            if (exprParameter == null && isConcreteType)
            {
                exprParameter = Expression.Parameter(parameterType, exprParamName);
                exprParameters.Add(exprParameter);
            }

            MemberExpression exprProperty;

            switch (uiExpression.ExpressionType)
            {
                case ExpressionType.Field :
                    exprProperty = Expression.Property(exprParameter, parameterType, uiExpression.ParameterValue.ToString());
                    return exprProperty;

                case ExpressionType.ConstantExpression :
                    var expression = Expression.Constant(uiExpression.ParameterValue);
                    return expression;

                case ExpressionType.EqualExpression :
                    exprProperty = Expression.Property(exprParameter, uiExpression.ParameterValue.ToString());
                    var expr = Expression.Equal(exprProperty, right);
                    return expr;

                default:
                    throw new Exception("Expression Type undefined");
            }
        }
    }

    public class AndLambdaExpression : LambdaExpressionBase
    {
        public override LambdaExpression Generate(UiExpression[] uiExpressions)
        {
            var lambdas = uiExpressions.Select(x => (Expression) Generate(x));

            var andExpression = lambdas.Aggregate((curr, next) =>
            {
                return Expression.And(curr, next);
            });

            var lambda = Expression.Lambda(andExpression, exprParameters);
            return lambda;
        }
    }
        
}
